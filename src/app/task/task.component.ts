import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AddAndEditTaskPopupComponent} from "../add-and-edit-task-popup/add-and-edit-task-popup.component";
import {MatDialog} from "@angular/material";
import {ViewTaskPopupComponent} from "../view-task-popup/view-task-popup.component";

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit, AfterViewInit {

  @Output() editTask = new EventEmitter<any>();

  @Output() closeTask = new EventEmitter<any>();

  @Output() recoveryTask = new EventEmitter<any>();

  @Input()
  task;
  alarmaFlag = false;
  deadlineFlag = false;

  constructor(public dialog: MatDialog,) {}

  ngOnInit() {
    let endDate = this.toZeroDate(this.task.endTime);
    let nowTime = this.toZeroDate(new Date);
    let alarmaTime = new Date(endDate.setDate(endDate.getDate() - 3));
    this.task.endTime = new Date(this.task.endTime.setDate(this.task.endTime.getDate() + 3));
    if (nowTime >= alarmaTime) {
      if (nowTime >=  this.task.endTime) {
        this.deadlineFlag = true
      } else {
        this.alarmaFlag = true;
      }
    }
  }

  ngAfterViewInit() {
  }

  handleEditTask() {
    this.editTask.emit(this.task);
  }

  handleCloseTask() {
    this.closeTask.emit(this.task);
  }

  handleRecoveryTask() {
    this.recoveryTask.emit(this.task);
  }

  toZeroDate(date) {
    date.setHours(12);
    date.setMinutes(0);
    date.setSeconds(0);
    return date;
  }

  openPopup(): void {
    const dialogRef = this.dialog.open(ViewTaskPopupComponent, {
      width: '697px',
      height: '671px',
      data: this.task,
    });
  }
}
