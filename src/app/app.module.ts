import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TaskBoardComponent } from './task-board/task-board.component';
import { TaskComponent } from './task/task.component';
import { AddAndEditTaskPopupComponent } from './add-and-edit-task-popup/add-and-edit-task-popup.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DragDropModule} from "@angular/cdk/drag-drop";
import {CdkTableModule} from "@angular/cdk/table";
import {CdkTreeModule} from "@angular/cdk/tree";
import {
  MatDatepickerModule,
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatNativeDateModule
} from "@angular/material";
import { ViewTaskPopupComponent } from './view-task-popup/view-task-popup.component';

@NgModule({
  declarations: [
    AppComponent,
    TaskBoardComponent,
    TaskComponent,
    AddAndEditTaskPopupComponent,
    ViewTaskPopupComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    DragDropModule,
    CdkTableModule,
    CdkTreeModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatDialogModule,
    MatInputModule,
    MatDatepickerModule,
  ],
  providers: [],
  entryComponents: [
    AddAndEditTaskPopupComponent,
    ViewTaskPopupComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
