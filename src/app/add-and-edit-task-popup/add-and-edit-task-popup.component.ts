import {Component, Inject, OnInit} from '@angular/core';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef
} from "@angular/material";
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from "@angular/material-moment-adapter";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'add-and-edit-task-popup',
  templateUrl: './add-and-edit-task-popup.component.html',
  styleUrls: ['./add-and-edit-task-popup.component.css'],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'ru-RU'},
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
  ],
})
export class AddAndEditTaskPopupComponent implements OnInit {

  now = new Date();

  taskForm = new FormGroup({
    title: new FormControl('') ,
    description: new FormControl(''),
    initialTime: new FormControl(new Date()),
    endTime: new FormControl(new Date()),
    oldStatus: new FormControl(null),
    status: new FormControl(0),
  });

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<AddAndEditTaskPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public task: any,
  ) {
    if (task) {
      this.initFields(task);
    }
  }


  ngOnInit() {
  }

  closePopup(data): void {
    this.dialogRef.close(data);
  }

  specifyEndDate(event) {
    this.taskForm.controls.endTime.setValue(event.value._d);
  }

  initFields(task) {
    for (let key in task) {
      this.taskForm.controls[key].setValue(task[key]);
    }
  }
}
