import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAndEditTaskPopupComponent } from './add-and-edit-task-popup.component';

describe('AddAndEditTaskPopupComponent', () => {
  let component: AddAndEditTaskPopupComponent;
  let fixture: ComponentFixture<AddAndEditTaskPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAndEditTaskPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAndEditTaskPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
