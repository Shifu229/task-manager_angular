import { Component, OnInit } from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {AddAndEditTaskPopupComponent} from "../add-and-edit-task-popup/add-and-edit-task-popup.component";
import {MatDialog} from "@angular/material";

@Component({
  selector: 'app-task-board',
  templateUrl: './task-board.component.html',
  styleUrls: ['./task-board.component.css']
})
export class TaskBoardComponent implements OnInit {

  draggedTask = null;

  newTasks = [
    {
      title: 'Задача 1',
      description: 'Задача 1 Задача 1 Задача 1 Задача 1 Задача 1 Задача 1 Задача 1 Задача 1 Задача 1 Задача 1 Задача 1 Задача 1 Задача 1',
      initialTime: new Date('Wed Jan 16 2019 02:28:57 GMT+0500'),
      endTime: new Date('Wed Jan 17 2019 02:28:57 GMT+0500'),
      oldStatus: null,
      status: 0,
    },
    {
      title: 'Задача 2',
      description: 'Задача 2 Задача 2 Задача 2 Задача 2 Задача 2 Задача 2',
      initialTime: new Date('Wed Jan 16 2019 02:28:57 GMT+0500'),
      endTime: new Date('Wed Jan 20 2019 02:28:57 GMT+0500'),
      oldStatus: null,
      status: 0,
    },
    {
      title: 'Задача 3',
      description: 'Задача 3 Задача 3 Задача 3 Задача 3 Задача 3 Задача 3 Задача 3 Задача 3 Задача 3',
      initialTime: new Date('Wed Jan 16 2019 02:28:57 GMT+0500'),
      endTime: new Date('Wed Jan 20 2019 02:28:57 GMT+0500'),
      oldStatus: null,
      status: 0,
    },
  ];

  workTasks = [
    {
      title: 'Задача 4',
      description: 'Задача 4 Задача 4 Задача 4 Задача 4 Задача 4 Задача 4 Задача 4',
      initialTime: new Date('Wed Jan 16 2019 02:28:57 GMT+0500'),
      endTime: new Date('Wed Jan 20 2019 02:28:57 GMT+0500'),
      oldStatus: null,
      status: 3,
    },
    {
      title: 'Задача 5',
      description: 'Задача 5 Задача 5 Задача 5 Задача 5 Задача 5 Задача 5 Задача 5 Задача 5 Задача 5',
      initialTime: new Date('Wed Jan 16 2019 02:28:57 GMT+0500'),
      endTime: new Date('Wed Jan 20 2019 02:28:57 GMT+0500'),
      oldStatus: null,
      status: 3,
    },
  ];

  completedTasks = [
    {
      title: 'Задача 6',
      description: 'Задача 6 Задача 6 Задача 6 Задача 6 Задача 6 Задача 6 Задача 6 Задача 6 Задача 6 Задача 6',
      initialTime: new Date('Wed Jan 16 2019 02:28:57 GMT+0500'),
      endTime: new Date('Wed Jan 20 2019 02:28:57 GMT+0500'),
      oldStatus: null,
      status: 5,
    }
  ];

  closedTasks = [];

  constructor(public dialog: MatDialog,) { }

  ngOnInit() {}

  openPopup(typeAction?): void {
    const dialogRef = this.dialog.open(AddAndEditTaskPopupComponent, {
      width: '697px',
      height: '671px',
      data: typeAction || false,
    });

    dialogRef.afterClosed().subscribe(res => {
      if (res !== undefined) {
        switch (res.status) {
          case 0: {
            let tmp = [];
            this.newTasks.forEach((task, i) => {
              if (task.initialTime === res.initialTime) {
                tmp.push(i);
              }
            });
            if (tmp.length === 0) {
              this.newTasks.push(res);
            } else {
              tmp.forEach(index => {
                this.newTasks.splice(index, 1, res);
              })
            }
            return;
          }

          case 3: {
            let tmp = [];
            this.workTasks.forEach((task, i)  => {
              if (task.initialTime === res.initialTime) {
                tmp.push(i);
              }
            });
            if (tmp.length === 0) {
              this.workTasks.push(res);
            } else {
              tmp.forEach(index => {
                this.workTasks.splice(index, 1, res);
              });
            }
            return;
          }

          case 5: {
            let tmp = [];
            this.completedTasks.forEach((task, i)  => {
              if (task.initialTime === res.initialTime) {
                tmp.push(i);
              }
            });
            if (tmp.length === 0) {
              this.completedTasks.push(res);
            } else {
              tmp.forEach(index => {
                this.completedTasks.splice(index, 1, res);
              });
            }
            return;
          }

          default: {
            return;
          }
        }
      }
    });
  }

  drop(event: CdkDragDrop<string[]>, status) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      this.draggedTask = null;
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
      this.draggedTask.oldStatus = this.draggedTask.status;
      this.draggedTask.status = status;
    }
  }

  handleClosedTasks(event) {
    switch (event.status) {
      case 0: {
        this.newTasks.forEach((task, i) => {
          if (task.initialTime === event.initialTime) {
            this.newTasks.splice(i, 1);
            event.oldStatus = event.status;
            event.status = 2;
            this.closedTasks.push(event)
          }
        });
        return;
      }

      case 3: {
        this.workTasks.forEach((task, i) => {
          if (JSON.stringify(task) === JSON.stringify(event)) {
            this.workTasks.splice(i, 1);
            event.oldStatus = event.status;
            event.status = 2;
            this.closedTasks.push(event)
          }
        });
        return;
      }

      case 5: {
        this.completedTasks.forEach((task, i) => {
          if (task.initialTime === event.initialTime) {
            this.completedTasks.splice(i, 1);
            event.oldStatus = event.status;
            event.status = 2;
            this.closedTasks.push(event)
          }
        });
        return;
      }

      default: {
        return;
      }
    }
  }

  handleRecoveredTask(event) {
    switch (event.oldStatus) {
      case 0: {
        this.closedTasks.forEach((task, i) => {
          if (task.initialTime === event.initialTime) {
            this.closedTasks.splice(i, 1);
            event.status = event.oldStatus;
            event.oldStatus = 2;
            this.newTasks.push(event)
          }
        });
        return;
      }

      case 3: {
        this.closedTasks.forEach((task, i) => {
          if (task.initialTime === event.initialTime) {
            this.closedTasks.splice(i, 1);
            event.status = event.oldStatus;
            event.oldStatus = 2;
            this.workTasks.push(event)
          }
        });
        return;
      }

      case 5: {
        this.closedTasks.forEach((task, i) => {
          if (task.initialTime === event.initialTime) {
            this.closedTasks.splice(i, 1);
            event.status = event.oldStatus;
            event.oldStatus = 2;
            this.completedTasks.push(event)
          }
        });
        return;
      }
      default: {
        return;
      }
    }
  }
}
