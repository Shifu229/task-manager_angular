import {Component, Inject, OnInit} from '@angular/core';
import {AddAndEditTaskPopupComponent} from "../add-and-edit-task-popup/add-and-edit-task-popup.component";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material";

@Component({
  selector: 'app-view-task-popup',
  templateUrl: './view-task-popup.component.html',
  styleUrls: ['./view-task-popup.component.css']
})
export class ViewTaskPopupComponent implements OnInit {

  alarmaFlag = false;
  deadlineFlag = false;

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<ViewTaskPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public task: any,
  ) { }

  ngOnInit() {
    let endDate = this.toZeroDate(this.task.endTime);
    let nowTime = this.toZeroDate(new Date);
    let alarmaTime = new Date(endDate.setDate(endDate.getDate() - 3));
    this.task.endTime = new Date(this.task.endTime.setDate(this.task.endTime.getDate() + 3));
    if (nowTime >= alarmaTime) {
      if (nowTime >=  this.task.endTime) {
        this.deadlineFlag = true
      } else {
        this.alarmaFlag = true;
      }
    }
  }

  closePopup(): void {
    this.dialogRef.close();
  }

  toZeroDate(date) {
    date.setHours(12);
    date.setMinutes(0);
    date.setSeconds(0);
    return date;
  }

}
