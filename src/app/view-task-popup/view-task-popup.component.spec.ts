import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTaskPopupComponent } from './view-task-popup.component';

describe('ViewTaskPopupComponent', () => {
  let component: ViewTaskPopupComponent;
  let fixture: ComponentFixture<ViewTaskPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTaskPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTaskPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
